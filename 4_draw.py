import cv2 as cv
import numpy as np

blank = np.zeros((500, 500, 3), dtype="uint8")  # Blank image
cv.imshow('Blank', blank)

# 1. Paint the image a certain color
# blank[200:300, 300:400] = 0, 255, 0  # Grün
# cv.imshow('Green', blank)

# 2. Draw a Rectangle
cv.rectangle(blank, (0,0), (250,500), (0,255,0), thickness=cv.FILLED) # Koordinaten: von 0,0 bis 250,250 mit der Farbe grün
cv.imshow('Rectangle', blank)

# 3. Draw a Circle
cv.circle(blank, (250,250), radius=40, color=(0,0,255), thickness=-1)
cv.imshow('Circle', blank)

# 4. Draw a line
cv.line(blank, (0,0), (blank.shape[1]//2, blank.shape[0]//2), (255,255,255), thickness=3)
cv.imshow('Line', blank)

# 5. Write text
cv.putText(blank, "Hello", (225,225), cv.FONT_HERSHEY_TRIPLEX, 1.0, (0,255,0), 2)
cv.imshow('Text', blank)

cv.waitKey(0)
