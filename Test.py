import cv2 as cv
import os

haar_cascade = cv.CascadeClassifier('haar_face.xml')

people = ['Ben Afflek', 'Elton John', 'Jerry Seinfield', 'Madonna', 'Mindy Kaling']
DIR = r'/Users/Hai/PycharmProjects/OpenCV_Tutorial/Faces/val/ben_afflek'
# features = np.load('features.npy')
# labels = np.load('labels.npy')

face_recognizer = cv.face.LBPHFaceRecognizer_create()
face_recognizer.read('face_trained.yml')

for i, img in enumerate(os.listdir(DIR)):
    img_path = os.path.join(DIR, img)

    img_array = cv.imread(img_path)
    gray = cv.cvtColor(img_array, cv.COLOR_BGR2GRAY)

    faces_rect = haar_cascade.detectMultiScale(gray, 1.1, 4)

    for (x, y, w, h) in faces_rect:
        faces_roi = gray[y:y + h, x:x + h]

        label, confidence = face_recognizer.predict(faces_roi)
        print(f'Label {i} = {people[label]} with a confidence of {confidence}')

        cv.putText(img_array, str(people[label]), (20, 20), cv.FONT_HERSHEY_COMPLEX, 1.0, (0, 255, 0), thickness=2)
        cv.rectangle(img_array, (x, y), (x + w, y + h), (0, 255, 0), thickness=2)

    cv.imshow(str(i), img_array)

cv.waitKey(0)
