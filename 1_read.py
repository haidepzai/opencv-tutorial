import cv2 as cv

# Photo:

img = cv.imread('Photos/cat_large.jpg')
cv.imshow('Cat', img)  # Window name and image
print(img.shape) # Height, Width, Farbkanal

cv.waitKey(0)  # wait for an infinite amount of time until a key is pressed

# Video:

# capture = cv.VideoCapture(0)
#
# while True:
#     isTrue, frame = capture.read()  # read video frame by frame
#     cv.imshow('Video', frame)
#
#     if cv.waitKey(20) & 0xFF == ord('d'):
#         break
#
# capture.release()
# cv.destroyAllWindows()
