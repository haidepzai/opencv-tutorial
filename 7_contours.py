import cv2 as cv
import numpy as np

img = cv.imread('Photos/Cat.jpg')
cv.imshow("Original", img)

blank = np.zeros(img.shape, dtype="uint8")
cv.imshow("Blank", blank)

gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
cv.imshow('Gray', gray)

blur = cv.GaussianBlur(gray, (5,5), cv.BORDER_DEFAULT)
cv.imshow("Blur", blur)

canny = cv.Canny(gray, 125, 175)
cv.imshow("Canny", canny)

# Biniraze image (black/white)
ret, thresh = cv.threshold(gray, 125, 255, cv.THRESH_BINARY) # Bild in Binär
cv.imshow("Thresh", thresh)

# contours = Liste mit dem Koordinaten der Konturen
contours, hierachies = cv.findContours(thresh, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
print(f'{len(contours)} contour(s) found!')

cv.drawContours(blank, contours, -1, (0,0,255), 2)
cv.imshow("Countours Drawn", blank)

cv.waitKey(0)