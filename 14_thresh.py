import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np

# Thresholding = binarization of an image (zero = black or 255 = white) pixel either black or white
# Thresholding Value and compare each pixel with that value; if above = white, if below = black
img = cv.imread('Photos/Cat.jpg')
cv.imshow("Original", img)

gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
cv.imshow("Gray", gray)

# Simple Thresholding
threshold, thresh = cv.threshold(gray, 150, 255, cv.THRESH_BINARY)  # Compare each pixel value with threshold value
# thresh = image; threshold = value
cv.imshow("Simple Thresholded", thresh)

# Inverse
threshold, thresh_inv = cv.threshold(gray, 150, 255, cv.THRESH_BINARY_INV)
cv.imshow("Inversed Thresholded", thresh_inv)

# Adaptive Thresholding
# Automatically find optimal thresholding value
adaptive_thresh = cv.adaptiveThreshold(gray, 255, cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY, 11, 3)
cv.imshow("Adaptive Thresholding", adaptive_thresh)

cv.waitKey(0)
