import cv2 as cv

# Photo:

# Reading image
img = cv.imread('Photos/cat_large.jpg')
cv.imshow('Cat', img)  # Window name and image


def rescaleFrame(frame, scale=0.75):
    # Works on photos, videos
    width = int(frame.shape[1] * scale)
    height = int(frame.shape[0] * scale)
    dimensions = (width, height)

    return cv.resize(frame, dimensions, interpolation=cv.INTER_AREA)


resized_image = rescaleFrame(img)
cv.imshow('Resized Image', resized_image)

# Video:

# Reading Videos
capture = cv.VideoCapture(0)

while True:
    isTrue, frame = capture.read()  # read video frame by frame

    frame_resized = rescaleFrame(frame, scale=.2)  # Rescaling 20%

    cv.imshow('Video', frame)
    cv.imshow('Video Resized', frame_resized)

    if cv.waitKey(20) & 0xFF == ord('d'):
        break


def changeRes(width, height):
    # Live Video
    capture.set(3, width)
    capture.set(4, height)


capture.release()
cv.destroyAllWindows()
